import json
import jsonhandler as js
import kalman
import math
import os
from pprint import pprint
import numpy as np
from sklearn import svm
from sklearn.calibration import CalibratedClassifierCV
from sklearn.neighbors.nearest_centroid import NearestCentroid
import matplotlib.pyplot as plt
from sklearn.naive_bayes import GaussianNB

np.set_printoptions(precision=4)
np.set_printoptions(threshold=np.nan)

#http://stackoverflow.com/questions/20113206/scikit-learn-svc-decision-function-and-predict

# with open('./fingerprint/0,0') as json_data:
#     beacons = json.load(json_data)
#     #pprint(beacons)
#
# #{"beacons":[{"beaconID":"8117:5","RSSI":[-85,-83,-85,-86
#
# #print(beacons["beacons"][0]["RSSI"])
# rssi = []
# for i in beacons['beacons']:
#    rssi.append(i['RSSI'])
# print(rssi[0])
# print(rssi[1])
def GetXY(cord):
    xpos = []
    ypos = []
    for item in cord:
        xpos.append(item[0])
        ypos.append(item[1])
    return xpos,ypos

def CalcError(real, pred):
    dist = []
    t=0
    for item in real:
        dist.append(math.hypot(int(real[t][0]) - pred[t][0], int(real[t][1]) - int(pred[t][1])))
        t = t+1
    return dist


#Kalman filter
def KalmanFilter(rssiList,addnoise):
    track=0
    for item in rssiList:
        rssiList[track] = kalman.kalmanFilter(item,addnoise)
        track = track + 1
    return


#Compensate for Missing BLE Signal
def CompensateforMissingBLE(refList, List1, List2):
    track = 0
    for item in refList:
        if item not in List1:
             #no item in the list
             empArr = [0]*len(List2[0])
             #print(empArr, List2[0], len(List2[0]))
             List2.insert(track,empArr)
        track = track + 1
    return

#sort list according to assending order
def sortList(refList, List1, List2):
    track = 0;
    for item in refList:
        if item in List1:
            i = List1.index(item)
            if (i!=track):
                #for swapping
                List1[track], List1[i] =  List1[i], List1[track]
                List2[track], List2[i] = List2[i], List2[track]
    track = track + 1
    return

#Finger printing array

fingerArray = []

#totalBLEList contains list in assending order
#BLEList contains list in random order as they are received while finger printing
totalBLEList, coordinateList = js.getCoordinateFromFile('./coordinates.json')
#print(totalBLEList)
#print(coordinateList)

#get all files in the folder first
fingerFiles = os.listdir('./fingerprint')
fingerpoints = np.zeros(len(fingerFiles))
counter = 0
fingerCords = []
for files in fingerFiles:
    BLEList = []
    RSSIList = []
    BLEList,RSSIList = js.getBeaconsFromFile('./fingerprint/' + files)
    # change to numpy array
    RSSIList = np.array(RSSIList,dtype=np.float32)
    #print(RSSIList)
    #for filling values for missing beacons
    CompensateforMissingBLE(totalBLEList, BLEList, RSSIList)
    sortList(totalBLEList, BLEList, RSSIList)
    #print(RSSIList)
    #KalmanFilter(RSSIList,True)
    fingerpoints[counter] = counter
    counter=counter+1
    RSSIList1 = kalman.MangifySamples(RSSIList,2, 2000)
    #print(RSSIList1)
    RSSIList1 = np.array(RSSIList1, dtype=np.float32)
    #transpose the matrix for finger printing
    RSSIList1 = RSSIList1.T
    fingerArray.append(RSSIList1)
    #get coordinates from filename
    fingerCords.append (files.split(',', 1 ))
##for getting coordinates from the file name

#exit()

##svm traning for coverting 3d array to  2d
fingerArray = np.asarray(fingerArray)
#print(fingerArray)
#exit()
nsamples, nx, ny = fingerArray.shape
#print(nsamples, nx, ny)
#for changing 3d to 2d array coz svm needs 2d array for training
d2_fingerArray = fingerArray.reshape((nsamples * nx,ny))

#print(len(d2_fingerArray))
#chaning repeating fringerprinting points also for xn times
finalFingerpoints = []
for items in fingerpoints:
    #for repeating same times nx times in array
    a = [items] * nx
    finalFingerpoints.extend(a)

#print(len(finalFingerpoints))

#print(d2_fingerArray)

clf = svm.SVC(kernel='linear', C = 1.0, probability=True)
clf1 =  NearestCentroid()
svm = svm.LinearSVC()
#clf = CalibratedClassifierCV(svm)
clf.fit(d2_fingerArray,finalFingerpoints)
clf1.fit(d2_fingerArray,finalFingerpoints)
#clfG = GaussianNB()
#clfG.fit(d2_fingerArray, finalFingerpoints)  # GaussianNB itself does not support sample-weights

##tile here training done
##test = d2_fingerArray[0:30]
##print(test)
##print(clf.predict(test))

##Now prediction starts

#get all files in the folder first
predictionFiles = os.listdir('./prediction')
predictionpoints = np.zeros(len(fingerFiles))
counter = 0
realCord = []
predCord = []
print(fingerFiles)
for files in predictionFiles:
     BLEList = []
     RSSIList = []
     BLEList, RSSIList = js.getBeaconsFromFile('./prediction/' + files)
     RSSIList = np.array(RSSIList, dtype=np.float32)
     CompensateforMissingBLE(totalBLEList, BLEList, RSSIList)
     sortList(totalBLEList, BLEList, RSSIList)
     RSSIList = np.asarray(RSSIList)
     KalmanFilter(RSSIList,False)
     # transpose the matrix for finger printing
     RSSIList = RSSIList.T
     #print(RSSIList)
     pred_position = clf.predict(RSSIList)
     pred_prob = clf.predict_proba(RSSIList)
     #print(clf1.predict(RSSIList))
     #print(pred_position)
     #print(pred_prob)
    #Gaussian Naive-Bayes with no calibration
     #prob_pos_clf = clfG.predict_proba(RSSIList)[:, 1]
     #print(clfG.predict_proba(RSSIList))
     ##now use algo

     ##getting real cordinates from  the file name
     realCord.append (files.split(',', 1 ))
     rowcount = 0
     x = []
     y = []
     for item in pred_position:
         x1 = 0
         y1 = 0
         colcount = 0
         for xy in fingerCords:
             #realCord[rowcount][colcount] = int(xy)
             x1 = x1 + int(xy[0]) * pred_prob[rowcount][colcount]
             y1 = y1 + int(xy[1]) * pred_prob[rowcount][colcount]
             colcount = colcount + 1
         x.append(x1)
         y.append(y1)
         rowcount = rowcount + 1
     # x and y contains final prediction coordinates
     x = np.asarray(x)
     y = np.asarray(y)
     predCord.append([x.mean(),y.mean()])
     #print(x.mean())
     #print(y.mean())
predCord=  np.round(predCord,2)
realCord = [[int(string) for string in inner] for inner in realCord]
#realCord=np.asarray(realCord)

print(realCord)
print(predCord)
error = CalcError(realCord, predCord)
error = np.asarray(error)
print(error)
print(error*45)
plt.figure(1)
#row, col = predCord
a,b = GetXY(realCord)
plt.scatter(a,b)
c,d = GetXY(predCord)
plt.plot(c,d,"r+")

plt.xlim(-1,)
plt.ylim(-1,)
#plt.show()

##each block is 45cm in the pavement os error is



