import json

#this function reads the json file and returns rssi and beacon list
def getBeaconsFromFile(filename):
    with open(filename) as json_data:
        beacons = json.load(json_data)
#iterate in json
        RSSIList = []
        BLEList = []

        for x in beacons['beacons']:
            RSSIList.append(x['RSSI'])
            BLEList.append(x['beaconID'])

    return BLEList, RSSIList

#this function reads coordinates from the json file and returns coordinate and coordinates
def getCoordinateFromFile(filename):
    with open(filename) as json_data:
        beacons = json.load(json_data)
#iterate in json
        coordinateList = []
        BLEList = []

        for x in beacons['Coordinates']:
            coord = [x['x'] , x['y']]
            coordinateList.append(coord)
            BLEList.append(x['beaconID'])
    return BLEList, coordinateList